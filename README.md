# Simple React and Java Spring Boot applications.
Homework for MS, Brno, 2016.


# React app 
- default url: http://localhost:3000
- require: js-base64, react, react-dom, react-scripts, react-bootstrap 

# Spring app
- default url: http://localhost:8080

# PostgreSQL database
- default url: http://localhost:5432/ms_homework_database
