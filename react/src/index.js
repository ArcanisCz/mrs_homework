import React, {Component} from "react";
import ReactDOM from "react-dom";
import UsersTable from "./userstable";
import {Button, FormGroup, FormControl, Form, Alert} from "react-bootstrap";


/**
 * Main component of index page with table of users.
 */
class DataStorage extends Component {

    /**
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            header: props.header,
            data: [],
            showUpdateRowComponent: false,
            updatedRowLabel: "",
            updatedRowId: -1,
            url: "http://localhost:8080/api",
            serverErrorMsg: ""
        };

        this.onRowDeleted = this.onRowDeleted.bind(this);
        this.onRowUpdated = this.onRowUpdated.bind(this);
        this.updateRowOkAction = this.updateRowOkAction.bind(this);
        this.updateRowCancelAction = this.updateRowCancelAction.bind(this);
        this.onRowAdded = this.onRowAdded.bind(this);
    }

    /**
     * @returns {XML} Table of Users with Add, Update and delete User components.
     */
    render() {
        var errorElement = <ErrorComponent msg={this.state.serverErrorMsg}/>;
        var addRowComponent = <AddUserComponent onRowAdded={this.onRowAdded}/>;

        var updateRowComponent = null;
        if (this.state.showUpdateRowComponent)
            updateRowComponent =
                <UpdateUserComponent okCallback={this.updateRowOkAction} cancelCallback={this.updateRowCancelAction}
                                     inputValue={this.state.updatedRowLabel} updatedRowId={this.state.updatedRowId}/>;

        return (
            <div>
                {errorElement}

                <UsersTable header={this.state.header} data={this.state.data} onRowDeleted={this.onRowDeleted}
                            onRowUpdated={this.onRowUpdated}/>

                {updateRowComponent}
                {addRowComponent}
            </div>);
    }

    /**
     * Load data after render.
     */
    componentDidMount() {
        this.loadData();
    }

    /**
     * Download list of users via. Rest API from database.
     */
    loadData() {
        var component = this;

        fetch(this.state.url + "/users", {
            method: "GET",
            headers: {
                'Accept': 'application/json'
            }
        }).then(function (response) {

            if (response.status === 200) {
                response.json().then(function (data) {
                    component.setState({data: data});
                });
                component.setState({serverErrorMsg: ""});
            } else {
                response.text().then(function (data) {
                    component.setState({serverErrorMsg: data});
                });
            }

        }, function (error) {
            component.setState({serverErrorMsg: error.message});
        })
    }

    /**
     * Callback for add new User.
     *
     * @param inputRef
     */
    onRowAdded(inputRef) {
        var component = this;
        var inputTextArea = ReactDOM.findDOMNode(inputRef);

        if (inputTextArea.value !== '') {

            fetch(this.state.url + "/user", {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': "application/json"
                },
                body: JSON.stringify({name: inputTextArea.value})
            }).then(function (response) {

                if (response.status === 200) {
                    response.json().then(function (data) {
                        component.state.data.push(data);
                        component.setState({data: component.state.data});
                        inputTextArea.value = "";
                        component.setState({serverErrorMsg: ""});
                    });
                } else {
                    response.text().then(function (data) {
                        component.setState({serverErrorMsg: data});
                    });
                }

            }, function (error) {
                component.setState({serverErrorMsg: error.message});
            })

        } else {
            alert("Input can not be blank.");
        }
    }

    /**
     * Callback for update User.
     *
     * @param id
     */
    onRowUpdated(id) {
        this.setState({updatedRowLabel: this.getUserNameByID(this.state.data, id)});
        this.setState({updatedRowId: id});
        this.setState({showUpdateRowComponent: true});
    }

    /**
     * Action after click to Ok in update User component.
     */
    updateRowOkAction(id, newName) {
        var component = this;

        if (newName !== '') {

            fetch(this.state.url + "/user", {
                method: "PUT",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': "application/json"
                },
                body: JSON.stringify({id: id, name: newName})
            }).then(function (response) {

                if (response.status === 200) {
                    response.json().then(function (data) {
                        component.setState({data: component.updateUserNameByID(component.state.data, data.id, data.name)});
                        component.setState({showUpdateRowComponent: false});
                        component.setState({serverErrorMsg: ""});
                    });
                } else {
                    response.text().then(function (data) {
                        component.setState({serverErrorMsg: data});
                    });
                }

            }, function (error) {
                component.setState({serverErrorMsg: error.message});
            })

        } else {
            alert("Input can not be blank.");
        }
    };

    /**
     * Action after click to Cancel in update User component.
     */
    updateRowCancelAction() {
        this.setState({showUpdateRowComponent: false});
    };

    /**
     * Callback for delete User.
     *
     * @param userID
     */
    onRowDeleted(userID) {
        var component = this;
        var Base64 = require('js-base64').Base64;

        fetch(this.state.url + "/secured/user", {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                 "Authorization": "Basic " + Base64.encode("tomaskubicek:tomaskubicek") // DELETE with auth of user Tomas Kubicek
            },
            body: JSON.stringify({id: userID, name: "not null name :)"})
        }).then(function (response) {

            if (response.status === 200) {
                component.setState({data: component.deleteUserByID(component.state.data, userID)});
                component.setState({serverErrorMsg: ""});
            } else {
                response.text().then(function (data) {
                    component.setState({serverErrorMsg: data});
                });
            }

        }, function (error) {
            component.setState({serverErrorMsg: error.message});
        })
    }

    /**
     * Delete User from array of object Users.
     *
     * @param arr
     * @param id
     * @returns {*} Array without deleted User by his ID.
     */
    deleteUserByID(arr, id) {
        var i = arr.length;

        while (i--)
            if (arr[i] && arr[i]['id'] === id)
                arr.splice(i, 1);

        return arr;
    }

    /**
     * @param arr
     * @param id
     * @returns {*} User from array of object Users.
     */
    getUserNameByID(arr, id) {
        var i = arr.length;

        while (i--)
            if (arr[i] && arr[i]['id'] === id)
                return arr[i]['name'];

        return "";
    }

    /**
     * Update User in array of object Users.
     *
     * @param arr
     * @param id
     * @param newName
     * @returns {*} Array with updated User by his ID.
     */
    updateUserNameByID(arr, id, newName) {
        var i = arr.length;

        while (i--)
            if (arr[i] && arr[i]['id'] === id)
                arr[i]['name'] = newName;

        return arr;
    }
}
DataStorage.PropTypes = {
    header: React.PropTypes.arrayOf(React.PropTypes.objectOf({
        key: React.PropTypes.string,
        label: React.PropTypes.string
    })).isRequired
};


/**
 * Component for add new User.
 */
class AddUserComponent extends Component {

    /**
     * @returns {XML} Div block with input text area and Ok button.
     */
    render() {
        return (
            <div id="inputAndButton">
                <Form inline>
                    <FormGroup>
                        <FormControl type="text" placeholder="New user" ref={(ref) => this.addUserInput = ref}/>
                    </FormGroup>
                    <Button bsStyle="success" onClick={() => this.props.onRowAdded(this.addUserInput)}>Add
                        user</Button>
                </Form>
            </div>
        );
    }
}
AddUserComponent.PropTypes = {
    onRowAdded: React.PropTypes.func.isRequired
};


/**
 * Component for update User.
 */
class UpdateUserComponent extends Component {

    /**
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {inputValue: props.inputValue};
    }

    /**
     * @returns {XML} Div block with input text area and Ok/Cancel buttons.
     */
    render() {
        return (
            <div id="inputAndButton">
                <Form inline>
                    <FormGroup>
                        <FormControl type="text" defaultValue={this.props.inputValue}
                                     onChange={(evt) => this.onInputChange(evt)}/>
                    </FormGroup>
                    <Button bsStyle="success"
                            onClick={()=> this.props.okCallback(this.props.updatedRowId, this.state.inputValue)}>Ok</Button>
                    <Button bsStyle="info" onClick={() => this.props.cancelCallback()}>Cancel</Button>
                </Form>
            </div>
        );
    }

    /**
     * @param event
     */
    onInputChange(event) {
        this.setState({inputValue: event.target.value});
    }
}
UpdateUserComponent.PropTypes = {
    okCallback: React.PropTypes.func.isRequired,
    cancelCallback: React.PropTypes.func.isRequired,
    inputValue: React.PropTypes.string,
    updatedRowId: React.PropTypes.number.isRequired
};


/**
 * Component for show error message.
 */
class ErrorComponent extends Component {

    render() {
        if (this.props.msg !== "")
            return (
                <Alert bsStyle="danger">
                    <h4>Error!</h4>
                    <p>{this.props.msg}</p>
                </Alert>
            );

        return null;
    }
}
ErrorComponent.PropTypes = {
    msg: React.PropTypes.string.isRequired
};


var header = [
    {key: 'id', label: 'ID'},
    {key: 'name', label: 'Fullname'},
    {key: 'action', label: 'Action'}
];

ReactDOM.render(<DataStorage header={header}/>, document.getElementById('container'));