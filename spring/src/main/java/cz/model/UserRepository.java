package cz.model;

import org.hibernate.annotations.SelectBeforeUpdate;
import org.hibernate.annotations.Subselect;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Tomas Kubicek, 2016.
 */
public interface UserRepository extends CrudRepository<User, Long> {

    /**
     * Return all Users who are stored in the database ordered by ID.
     *
     * @return
     */
    List<User> findAllByOrderById();

    /**
     * Find and return User by his ID.
     *
     * @param id
     * @return
     */
    //@Query(value= "SELECT * FROM Users WHERE id = ?1", nativeQuery = true)
    @Query("SELECT u FROM User u WHERE u.id = ?1")
    User getUserById(long id);

    /**
     * Find and return User by his username.
     *
     * @param username
     * @return
     */
    User findByUsername(String username);
}
